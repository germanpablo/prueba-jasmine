describe("cuando llamo al metodo procesar del objeto analizador", function() {
  it("ejecuta el metodo aproba del objeto procesador", function() {
      spyOn(procesador.prototype, 'aproba');
      procesar();
      expect(procesador.prototype.aproba).toHaveBeenCalled();
  });   

  
   it("ejecuta el metodo rechaza del objeto procesador", function() {				
	 	 spyOn(procesador.prototype, 'rechaza');
      procesarDos();
      expect(procesador.prototype.rechaza).toHaveBeenCalled();
  });  
});


